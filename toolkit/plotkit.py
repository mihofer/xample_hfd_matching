import matplotlib.pyplot as plt

def compare_twiss(df1, df2):
    fig, ax = plt.subplots(nrows=4, ncols=1, figsize=(9,12), sharex=True)

    ax[0].plot(df1['s'], df1['betx'], color='red')
    ax[0].plot(df1['s'], df1['bety'], color='blue')
    ax[0].plot(df2['s'], df2['betx'], color='red', linestyle='--')
    ax[0].plot(df2['s'], df2['bety'], color='blue', linestyle='--')
    ax[0].set_ylabel(r'$\beta~[m]$')

    ax[1].plot(df1['s'], df1['dx'], color='lime')
    ax[1].plot(df1['s'], df1['dy'], color='violet')
    ax[1].plot(df2['s'], df2['dx'], color='lime', linestyle='--')
    ax[1].plot(df2['s'], df2['dy'], color='violet', linestyle='--')
    ax[1].set_ylabel(r'$Dispersion~[m]$')

    ax[2].plot(df1['s'], df1['x'], color='black')
    ax[2].plot(df1['s'], df1['y'], color='gray')
    ax[2].plot(df2['s'], df2['x'], color='black', linestyle='--')
    ax[2].plot(df2['s'], df2['y'], color='gray', linestyle='--')
    ax[2].set_ylabel(r'$Orbit~[m]$')

    ax[3].plot(df1['s'], df1['delta'], color='orange')
    ax[3].plot(df2['s'], df2['delta'], color='orange', linestyle='--')
    ax[3].set_ylabel(r'$\delta_p$')
    ax[3].set_xlabel(r's [m]')